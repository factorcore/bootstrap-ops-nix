# Function to check basic AWS CLI configuration
check_aws_configuration() {
    if [[ ! -f ~/.aws/config ]] || [[ ! -f ~/.aws/credentials ]]; then
        echo "AWS CLI is not configured."
        return 1
    else
        echo "AWS CLI is already configured."
        return 0
    fi
}

# Function to configure AWS CLI (Basic Configuration)
configure_aws_cli_sso() {
    read -p "Enter your SSO start URL: " SSO_START_URL
    read -p "Enter your SSO region: " SSO_REGION

    # Prompt for the profile name to configure or use a default
    read -p "Enter the profile name to configure for SSO (default 'default'): " PROFILE_NAME
    PROFILE_NAME=${PROFILE_NAME:-default}

    echo "Configuring AWS CLI for SSO..."
    # Set the SSO parameters for the specified profile
    aws configure set sso_start_url "$SSO_START_URL" --profile "$PROFILE_NAME"
    aws configure set sso_region "$SSO_REGION" --profile "$PROFILE_NAME"
    # Optionally, set other required parameters like sso_account_id and sso_role_name if you have them
    # aws configure set sso_account_id "YOUR_SSO_ACCOUNT_ID" --profile "$PROFILE_NAME"
    # aws configure set sso_role_name "YOUR_SSO_ROLE_NAME" --profile "$PROFILE_NAME"

    echo "SSO configuration set for profile '$PROFILE_NAME'."
        echo "Attempting to initiate SSO login process..."

    # Automatically initiate the SSO login process
    aws sso login --profile "$PROFILE_NAME"

    # Check if the SSO login was successful by attempting a test command that requires authentication
    if aws sso list-accounts --profile "$PROFILE_NAME" &> /dev/null; then
        echo "SSO login successful for profile '$PROFILE_NAME'."
    else
        echo "SSO login failed or was not completed for profile '$PROFILE_NAME'. Please try running 'aws sso login --profile $PROFILE_NAME' manually."
    fi
}

# Main script logic

# Step 1: Check for AWS CLI and install if necessary
if ! check_aws_cli; then
    # Verify installation success before proceeding
    if ! check_aws_cli; then
        echo "Failed to install AWS CLI. Exiting."
        exit 1
    fi
fi

# Step 2: Check if AWS CLI is configured, and configure if necessary
if ! aws sso list-accounts --output json &> /dev/null; then
    configure_aws_cli_sso
else
    echo "AWS SSO is already configured or your session is active."
fi

# Fetch and loop through all accounts accessible via SSO
accounts=$(aws sso list-accounts --output json | jq -c '.accountList[]')

if [ -z "$accounts" ]; then
    echo "No accounts found or unable to retrieve accounts with SSO. Ensure you're logged in with 'aws sso login'."
    exit 1
fi

echo "Generating AWS CLI profiles for accessible SSO accounts..."

# Loop through each account JSON blob
echo "$accounts" | while read -r account; do
    account_id=$(echo "$account" | jq -r '.accountId')
    account_name=$(echo "$account" | jq -r '.accountName' | sed 's/ /_/g')

    # Here, specify the role name you want to assume in each account
    # This example assumes a role named 'YourRoleNameHere' exists in all accounts
    role_name="YourRoleNameHere"

    profile_name="${account_name}_${role_name}"

    # Configure the AWS CLI SSO profile for this account and role
    aws configure set profile.$profile_name.sso_account_id "$account_id"
    aws configure set profile.$profile_name.sso_region "your-sso-region"
    aws configure set profile.$profile_name.sso_role_name "$role_name"
    aws configure set profile.$profile_name.sso_start_url "your-sso-start-url"

    echo "Profile created: $profile_name"
done

echo "All accessible SSO accounts have been configured with profiles."