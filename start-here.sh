#!/bin/bash

set -e

# Check if Nix is installed
if ! command -v nix-shell &> /dev/null
then
    echo "Nix is not installed. Installing..."
    # Install Nix
    sh <(curl -L https://nixos.org/nix/install) --no-daemon
    # Source nix commands for the current session
    . /etc/profile.d/nix.sh
else
    echo "Nix is already installed."
fi

# Ensure the Nix profile is sourced, in case it's a fresh install
if [ -e ~/.nix-profile/etc/profile.d/nix.sh ]; then
    . ~/.nix-profile/etc/profile.d/nix.sh
fi


TARGET_DIR="$HOME/.ops"

if [ ! -d "$TARGET_DIR" ]; then
    mkdir -p "$TARGET_DIR"
    # Assuming the script is run from the root of the cloned repo
    cp -r $(pwd) "$TARGET_DIR"
    echo "Copied repository to $TARGET_DIR"
else
    echo "Setup directory already exists at $TARGET_DIR"
fi
if ! grep -q '. ~/.nix-profile/etc/profile.d/nix.sh' "$HOME/.bashrc"; then
    echo '. ~/.nix-profile/etc/profile.d/nix.sh' >> "$HOME/.bashrc"
    echo "Added Nix profile sourcing to .bashrc"
fi
alias ops="nix-shell $HOME/.ops/bootstrap-ops-nix/devops.nix --pure"

echo "run ops to get started"