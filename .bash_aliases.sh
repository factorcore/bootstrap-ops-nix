alias ssm="aws ssm start-session --target"
alias ec2="aws ec2"
alias tf=terraform
alias tg=terragrunt
alias awsl="aws sso login"
alias hg="history | grep"
alias kvu="kubectl view-utilization -h"
alias k=kubectl
alias synchw="sudo hwclock -s"
alias ops="nix-shell $HOME/.ops/bootstrap-ops-nix/devops.nix --pure"
# Set AWS Profile
init() {
  if [ $1 ]; then
    awsl
    gawsl
    profile $1
    region $2
    context $3
  else
    echo "Authenticate with AWS Commercial"
    awsl
    echo "Authenticate with AWS GovCloud"
    gawsl
    echo "Select an Aws Profile"
    profile
    echo "Select default AWS Region"
    region
    echo "Select a Kubeconfig Context"
    context
  fi
}
profile() {
    if [ $1 ]; then
    export ACTIVE_ENV="$1"
    export AWS_PROFILE="$1"
  else
    TYPE=$(gum choose $(aws configure list-profiles))
    export ACTIVE_ENV="$TYPE"
    export AWS_PROFILE="$TYPE"
  fi
}
# Set K8 Context
context() {
    if [ $1 ]; then
      export KUBE_CONTEXT=$1
      kubectl config use-context $1
    else
      TYPE=$(gum choose $(kubectl config get-contexts -o name))
      export KUBE_CONTEXT=$TYPE
      kubectl config use-context $TYPE
    fi
}
namespace() {
  if [ $1 ]; then
    kubectl config set-context $KUBE_CONTEXT --namespace $1
  else
    TYPE=$(gum choose $(kubectl get namespaces))
    kubectl config set-context $KUBE_CONTEXT --namespace $TYPE
  fi
}
region() {
  if [ $1 ]; then
    export AWS_DEFAULT_REGION="$1"
  else
    REGION=$(gum spin --show-output -s globe --title 'Loading Regions...' -- aws ec2 describe-regions | jq -r '.Regions | to_entries[] | .value.RegionName' | gum filter --placeholder "Pick a region")
    export AWS_DEFAULT_REGION="$REGION"
  fi
}
contextadd() {
  profile
  region
  name=$(gum choose $(aws eks list-clusters --output text --query 'clusters[*]'))
  aws eks update-kubeconfig --name $name --alias $name
}
#Choose Product

ssd() {
  id=$(agi)
  aws ssm start-session --target $id
}
instance_filter() {
  name=$(ec2 describe-instances --output text --filters "Name=instance-state-name,Values=running" --query 'Reservations[*].Instances[*].{Tags:Tags[?Key == `Name`] | [0].Value}'| gum filter)
}
agi() {
    name=$(ec2 describe-instances --output text --filters "Name=instance-state-name,Values=running" --query 'Reservations[*].Instances[*].{Tags:Tags[?Key == `Name`] | [0].Value}' | gum filter)
    eid $name
}
eid() {
if [ -n "$1" ]; then
  echo $(ec2 describe-instances --filters "Name=tag:Name,Values=$1" --output text --query 'Reservations[*].Instances[*].InstanceId')
else
  echo "Please Specify a Value for tag:Name"
fi
}

#PortForward
pf() {
if [ -n "$1" ]; then
  ssm $1 --document-name AWS-StartPortForwardingSession --parameters "portNumber=[$2],localPortNumber=[$3]"
else
  echo "usage: pfp instance_id remote-port local-port"
fi
}

pfr() {
if [ -n "$1" ]; then
aws ssm start-session --target $1 --document-name AWS-StartPortForwardingSessionToRemoteHost --parameters '{"portNumber":[$2],"localPortNumber":[$3],"host":[$4]}'
else
  echo "usage: pfr instance_id remote-port local-port remote-host"
fi
}