#!/usr/bin/env bash

cp $HOME/.ops/bootstrap-ops-nix/.bash_aliases.sh ~/.bash_aliases
cp $HOME/.ops/bootstrap-ops-nix/starship.toml ~/.config/starship.toml
cp $HOME/.ops/bootstrap-ops-nix/.tool-versions ~/.tool-versions
