{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.git
    pkgs.curl
    pkgs.asdf-vm
    pkgs.starship
    pkgs.gum
    pkgs.cacert
    pkgs.unzip

  ];

  shellHook = 
  ''
    source $HOME/.ops/bootstrap-ops-nix/setup-asdf.sh
    source $HOME/.ops/bootstrap-ops-nix/copy-files.sh
    source $HOME/.ops/bootstrap-ops-nix/generate-aws-profiles.sh
    source $HOME/.ops/bootstrap-ops-nix/generate-kubeconfig.sh
    eval "$(starship init bash)"
    . "$HOME/.asdf/asdf.sh"
  '';
}