#!/usr/bin/env bash

declare -A plugins=(
    [packer]="https://github.com/asdf-community/asdf-hashicorp.git"
    [terraform]="https://github.com/asdf-community/asdf-hashicorp.git"
    [vault]="https://github.com/asdf-community/asdf-hashicorp.git"
    [terragrunt]="https://github.com/ohmer/asdf-terragrunt"
    [kubectl]="https://github.com/asdf-community/asdf-kubectl.git"
    [cmctl]="https://github.com/asdf-community/asdf-cmctl.git"
    [grpcurl]="https://github.com/asdf-community/asdf-grpcurl.git"
    [tridentctl]="https://github.com/asdf-community/asdf-tridentctl.git"
    [awscli]="https://github.com/MetricMike/asdf-awscli.git"  # Add more plugins here
)

# Install ASDF plugins if they are not already installed
for plugin in "${!plugins[@]}"; do
  if [ -z "$(asdf plugin-list | grep -w $plugin)" ]; then
    url=${plugins[$plugin]}
    if [ -n "$url" ]; then
      asdf plugin-add $plugin $url
    else
      asdf plugin-add $plugin
    fi
  fi
done

# Update all installed plugins
#asdf plugin-update --all

# Install and set the latest version for each plugin
# Install and set the latest version for each plugin
for plugin in "${!plugins[@]}"; do
  latest_version=$(asdf list-all $plugin | grep -v '[a-zA-Z]' | tail -1)
  if [ -z "$latest_version" ]; then
    echo "Could not determine the latest version for $plugin."
    continue
  fi
  echo "Installing $plugin $latest_version"
  asdf install $plugin $latest_version
  asdf global $plugin $latest_version
done
echo "-----------------"

echo "All specified plugins have been installed and set to their latest versions."
