# Specify the path to your central kubeconfig file. You can use the default location (~/.kube/config) or a custom path.
KUBECONFIG_PATH="$HOME/.kube/config"
rm $KUBECONFIG_PATH

export KUBECONFIG="$KUBECONFIG_PATH"
for profile in $(aws configure list-profiles); do
    echo "Processing profile: $profile"
    
    clusters=$(aws eks list-clusters --profile "$profile" --output text --query 'clusters[*]')
    
    if [ -z "$clusters" ]; then
        echo "No clusters found for profile $profile."
        continue
    fi
    
    for cluster in $clusters; do
            # Extract the fourth word from the cluster name
        IFS='-' read -ra ADDR <<< "$cluster"
        if [ ${#ADDR[@]} -ge 4 ]; then
            fourth_word=${ADDR[3]}
            alias_name="${fourth_word}-${profile}"
        else
            echo "The cluster name does not have four words separated by dashes. Skipping."
            continue
        fi

        echo "Adding cluster $cluster to the kubeconfig"
        
        # Use the default kubeconfig location or a specified path. This updates or appends the cluster configuration.
        aws eks update-kubeconfig --name "$cluster" --profile "$profile" --alias "$alias_name"
        
        echo "Cluster $cluster added."
    done
done

echo "All clusters are configured in the kubeconfig. $KUBECONFIG_PATH"
